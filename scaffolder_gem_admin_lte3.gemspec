Gem::Specification.new do |s|
  s.name = %q{scaffolder_gem_admin_lte3}
  s.version = "1.4.4"
  s.date = %q{2018-11-22}
  s.summary = %q{scaffolder for AdminLTE3}
  s.description = "A gem that overrides the default scaffolder, so the views and controllers match the ones used in AdminLTE"
  s.authors = %q{Luka Bizant}
  s.email = 'luka@prelom.digital'
  s.homepage = 'https://prelom.digital'
  s.files = [
    "lib/rails/generators/erb/scaffold/scaffold_generator.rb",
    "lib/templates/active_record/model/model.rb",
    "lib/templates/rails/scaffold_controller/controller.rb"
  ]
  s.require_paths = ["lib", "lib/templates", "lib/generators"]
  s.license       = 'MIT'
end