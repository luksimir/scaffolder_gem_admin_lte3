
module ScaffolderGem

  # Default way to set up ScaffolderGem. Run rails generate scaffolder_gem:install to create
  # a fresh initializer with all configuration values.
  def self.setup
    yield self
  end
end
