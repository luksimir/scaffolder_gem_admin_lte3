# frozen_string_literal: true
module ScaffolderGem
  module Generators
    class InstallGenerator < Rails::Generators::Base
      desc "Copy files to lib"
      source_root File.expand_path('../templates', __FILE__)

      def info_bootstrap
        #puts "IMA"
      end

      def copy_config
        #puts "IMA"
      end

      def copy_scaffold_template
        puts "Copy view templates"
        copy_file "_form.html.erb", "lib/templates/erb/scaffold/_form.html.erb"
        copy_file "edit.html.erb", "lib/templates/erb/scaffold/edit.html.erb"
        copy_file "filters.html.erb", "lib/templates/erb/scaffold/filters.html.erb"
        copy_file "index.html.erb", "lib/templates/erb/scaffold/index.html.erb"
        copy_file "index.js.erb", "lib/templates/erb/scaffold/index.js.erb"
        copy_file "new.html.erb", "lib/templates/erb/scaffold/new.html.erb"
        copy_file "plural.html.erb", "lib/templates/erb/scaffold/plural.html.erb"
        copy_file "show.html.erb", "lib/templates/erb/scaffold/show.html.erb"
        copy_file "singular.html.erb", "lib/templates/erb/scaffold/singular.html.erb"

        puts "Copy model template"
        copy_file "model.rb", "lib/templates/active_record/model/model.rb"

        puts "Copy controller template"
        copy_file "controller.rb", "lib/templates/rails/scaffold_controller/controller.rb"

        puts "Copy scaffold generator"
        copy_file "scaffold_generator.rb", "lib/rails/generators/erb/scaffold/scaffold_generator.rb"

      end

      def show_readme
        readme "README"
      end
    end
  end
end
